﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;

namespace Newsery.HtmlParser
{
    interface IWebsiteCrawler
    {
        List<Article> Crawl(string startURL, string childLinksRegularExpression);
    }
}
