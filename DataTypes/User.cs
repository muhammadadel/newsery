﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Newsery.DataTypes
{
    public class User
    {
        private const int SALT_BYTE_SIZE = 24;
        private const int VERIFICATION_KEY_BYTE_SIZE = 24;
        private const int RFC2898_ITERATIONS_COUNT = 30000;
        private const int HASH_BYTE_SIZE = 24;

        public string Email { get; set; }
        public string Salt { get; set; }
        public string Password { get; set; }
        public string HashedSaltedPassword { get; set; }
        public string Name{get;set;}
        public string EmailVerificationKey { get; set; }
        public DateTime KeyGenerationTime { get; set; }
        public bool EmailVerified { get; set; }

        

        public static string GenerateSalt()
        {
            RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[SALT_BYTE_SIZE];
            csprng.GetBytes(salt);
            return Convert.ToBase64String(salt);
        }

        public static  string GenerateEmailVerificationKey()
        {
            RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider();
            byte[] verificationKey = new byte[VERIFICATION_KEY_BYTE_SIZE];
            csprng.GetBytes(verificationKey);
            return Convert.ToBase64String(verificationKey);
        }
        public static string GetHashedSaltedPassword(string password,string salt)
        {
            byte[] saltBytes = Convert.FromBase64String(salt);
            Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, saltBytes,RFC2898_ITERATIONS_COUNT);
            return Convert.ToBase64String(pbkdf2.GetBytes(HASH_BYTE_SIZE));
        }
    }
}
