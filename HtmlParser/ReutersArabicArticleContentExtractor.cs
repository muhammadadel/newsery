﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;

namespace Newsery.HtmlParser
{
    public class ReutersArabicArticleContentExtractor : IHtmlArticleContentExtractorInterface
    {
        public bool Parse(Article article)
        {
             NReadabilityArticleContentExtractor contentExtractor = new NReadabilityArticleContentExtractor();
            bool result = contentExtractor.Parse(article);
            if (result)
            {
                int index = article.Content.IndexOf("(إعداد");
                if (index != -1)
                    article.Content = article.Content.Remove(index);
            }
            return result;
        }
        }
    }

