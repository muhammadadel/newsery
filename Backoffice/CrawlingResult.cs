﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newsery.Backoffice
{
    public class CrawlingResult
    {
        public int ArtilcesProcessedCount { get; set; }
        public string NewsSoruce{get;set;}
        public bool Succeeded { get; set; }
        public string ErrorMessage { get; set; }
    }
}
