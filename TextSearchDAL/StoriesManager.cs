﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;
using Nest;

namespace Newsery.TextSearchDAL
{
    public class StoriesManager:ElasticSearchClient
    {
        public StoriesManager(string indexName)
        {
            _ConnectionSettings.SetDefaultIndex(indexName);
        }
        public List<Story> GetStories(int numberOfStories)
        {
            var response = Client.Search<Story>(s => s.MatchAll().SortDescending(t => t.Date).SortAscending(t => t.Score).Take(numberOfStories));

            return response.Documents.ToList();
        }
        public Story GetStoryById(string id)
        {
            var response = Client.Search<Story>(s => s.Query(q => q.Term("id", id)));
            if (response.Documents.Count() > 0)
                return response.Documents.First();
            return null;
        }
    }
}
