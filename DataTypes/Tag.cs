﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newsery.DataTypes
{
    
    public class Tag
    {
        public Guid Id
        {
            get
            {
                if (_Id == new Guid())
                {
                    _Id = Guid.NewGuid();
                }
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }
        public uint Order { get; set; }
        public string Value { get; set; }
        public string Country { get; set; }
        private Guid _Id;
        


    }
}
