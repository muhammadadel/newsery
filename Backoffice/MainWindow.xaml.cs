﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Runtime.Remoting.Messaging;
using System.ComponentModel;

using Newsery.DataTypes;
using Newsery.HtmlParser;
using Newsery.TextSearchDAL;
using Newsery.CassandraDataAccessLayer;
using System.Configuration;

namespace Newsery.Backoffice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string CountryToCrawl;
        private List<NewsSource> Sources;
        /// <summary>
        /// The variable used to start and stop crawling
        /// </summary>
        private bool CrawlerRunning;
        int NextSourceToCrawlIndex;
        public MainWindow()
        {
            InitializeComponent();

        }

        private void CrawlNextNewsSource()
        {
            if (!CrawlerRunning)
                return;
            if (Sources.Count <= NextSourceToCrawlIndex)
            {
                //start once again
                txtOutput.Text = "";
                StartCrawling();
                return;
            }
            NewsSource source = Sources[NextSourceToCrawlIndex];
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.DoWork += worker_DoWork;
            worker.RunWorkerAsync(source);

            txtOutput.Text += "\n Started crawling " + source.Url;
        }
        private void menuItemCrawleEgypt_Click(object sender, RoutedEventArgs e)
        {
            CountryToCrawl = "Egypt";
            StartCrawling();
        }
        private void menuItemCrawlUS_Click(object sender, RoutedEventArgs e)
        {
            CountryToCrawl = "US";
            StartCrawling();
        }


        private void menuItemStartCrawler_Click(object sender, RoutedEventArgs e)
        {
            CountryToCrawl = "";
            StartCrawling();  
        }

        private void StartCrawling()
        {
            CrawlerRunning = true;
            if (CountryToCrawl=="")
                Sources = NewsSroucesManager.GetNewsSources().ToList();
            else
                Sources = NewsSroucesManager.GetNewsSourcesByCountryName(CountryToCrawl).ToList();
            if (Sources.Count > 0)
            {
                NextSourceToCrawlIndex = 0;
                CrawlNextNewsSource();
            }
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            CrawlingResult result = (CrawlingResult)e.Result;
            if (result.Succeeded)
                txtOutput.Text += "\n " + result.ArtilcesProcessedCount.ToString() + " Artilces Parsed from " + result.NewsSoruce;
            else
            {
                txtOutput.Text += "\n " +" Parsing failed for " + result.NewsSoruce + "\n";
                txtOutput.Text += result.ErrorMessage;
            }
            NextSourceToCrawlIndex++;
            CrawlNextNewsSource();
            
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            NewsSource source = (NewsSource)e.Argument; 
          
            try
            {
                int indexedArticlesCount = ProcessNewsSource(source);
                CrawlingResult result = new CrawlingResult();
                result.ArtilcesProcessedCount = indexedArticlesCount;
                result.NewsSoruce = source.Url;
                result.Succeeded = true;
                e.Result = result;
            }
            catch (Exception ex)
            {
                CrawlingResult result = new CrawlingResult();
                result.NewsSoruce = source.Url;
                result.Succeeded = false;
                result.ErrorMessage = ex.Message;
                e.Result = result;
            }
            
        }

        private  int ProcessNewsSource(NewsSource source)
        {
            bool UseNewsSourceDateTime = bool.Parse(ConfigurationManager.AppSettings["UseNewsSourceDateTime"]);
            INewsSourceParser parser = NewsSourceParserResolver.GetParser(source);

            List<Article> articles = parser.Parse(source);
            
            foreach (Article a in articles)
            {
                if (source.TagValue !=null)
                    a.Tags.Add(source.TagValue);
                a.NewsPaper = source.NewspaperName;
                a.NewsSrouceCountry = source.Country;
                if (!UseNewsSourceDateTime)
                    a.Date = DateTime.UtcNow;
            }
            int indexedArticlesCount = 0;
            ParallelOptions options = new ParallelOptions();
            options.MaxDegreeOfParallelism = 1;            
            Parallel.ForEach(articles,options, a =>
            {
                IndexingPipeline pipeline = Pool.GetPipeline();
               
                    if (pipeline.Index(a))
                        indexedArticlesCount++;

                    Pool.ReturnPipeline(pipeline);
            }
             );
            return indexedArticlesCount;
        }

        private  bool ExtractArticleContent(Article a)
        {
            IHtmlArticleContentExtractorInterface contentExtractor =  ArticleContentExtractorResolver.GetExtractor(a);
            bool parsingResult = contentExtractor.Parse(a);
            return parsingResult;
        }
       

        private void menuItemStopCrawler_Click(object sender, RoutedEventArgs e)
        {
            CrawlerRunning = false;
            txtOutput.Text += "\n Crawling terminated ";

        }

        private void menuItemCreateIndices_Click(object sender, RoutedEventArgs e)
        {
            Initializer initializer = new Initializer();
            initializer.Initialize();
        }

        private void menuItemCreateCassandraSchema_Click(object sender, RoutedEventArgs e)
        {
            SchemaCreator.CreateSchema1_0();
        }

        

        
       
      
       
       
    }
}
