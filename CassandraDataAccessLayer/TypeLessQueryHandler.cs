﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraSharp;
using CassandraSharp.CQLPropertyBag;
using CassandraSharp.CQLCommand;

using CassandraSharp.CQLPoco;

namespace Newsery.CassandraDataAccessLayer
{
    //execute a query without knowing the 
    public class TypeLessQueryHandler
    {
        public static List<Dictionary<string,object>> ExecuteQuery(string Query)
        {
            ICluster cluster = CassandraClient.Cluster;
            IPropertyBagCommand cmd = cluster.CreatePropertyBagCommand();
            var v= cmd.Execute(Query).AsFuture();
            v.Wait();
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            foreach (PropertyBag p in v.Result)
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                foreach (string key in p.Keys)
                {
                    d[key] = p[key];
                }
                result.Add(d);
            }
            return result;
        }
    }
}
