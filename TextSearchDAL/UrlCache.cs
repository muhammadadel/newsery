﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newsery.TextSearchDAL
{
    /// <summary>
    /// A class for quickly finding if a url was indexed or not. This is much faster than asking elastic search if an article with a given url exists
    /// </summary>
    class UrlCache
    {
        private object ThreadAccessLock = new object();
        public UrlCache(int size)
        {
            Size = size;
            UrlsHashSet = new HashSet<string>();
        }
        private int Size { get; set; }
        public bool KeyExists(string key)
        {
            lock (ThreadAccessLock)
            {
                if (UrlsList.Contains(key))
                    return true;
                return false;
            }
        }
        public void AddKey(string url)
        {
            lock (ThreadAccessLock)
            {
                if (UrlsHashSet.Count == Size)
                {
                    var ItemsToBeRemoved = UrlsList.Take(Size / 10);
                    foreach (string urlToBeRemoved in ItemsToBeRemoved)
                        UrlsHashSet.Remove(urlToBeRemoved);
                    UrlsList.RemoveRange(0, Size / 10);
                }
                UrlsHashSet.Add(url);
                UrlsList.Add(url);
            }
        }
        /// <summary>
        /// List used to maintain order of added urls since hashset doesn't maintain order. We are working on FIFO bases
        /// </summary>
        private List<string> UrlsList = new List<string>();
        private HashSet<string> UrlsHashSet;
    }
}
