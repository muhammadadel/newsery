﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newsery.HtmlParser
{
    public class DefaultArticleSummarizer:IArticleSummarizer
    {
        public string Summarize(string articleContent)
        {
            string[] statements = articleContent.Split(new char[] { '\n' });
            StringBuilder summaryStringBuilder = new StringBuilder();
            for (int i = 0; i < statements.Length; i++)
            {
                if (statements[i].Length < 25)
                    //this is a very short statements, most propably the name of the author.
                    continue;
                summaryStringBuilder.Append(statements[i]);
                if (summaryStringBuilder.Length < 200)
                    summaryStringBuilder.Append("\n");
                else
                    break;
            }
            return summaryStringBuilder.ToString();
        }
    }
}
