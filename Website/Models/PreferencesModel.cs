﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newsery.DataTypes;
namespace Newsery.Website
{
    public class SelectedTag
    {
        public string Country { get; set; }
        public string Id { get; set; }
        public bool IsSelected { get; set; }
        public string Value { get; set; }
    }
    public class SelectedNewspaper
    {
        public string Country { get; set; }
        public string Id { get; set; }
        public bool IsSelected { get; set; }
        public string Name { get; set; }
    }
    public class PreferencesModel
    {
        public List<SelectedTag> SelectedTags { get; set; }
        public List<SelectedNewspaper> SelectedNewspapers { get; set; }
    }
}