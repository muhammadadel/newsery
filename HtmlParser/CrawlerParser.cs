﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace Newsery.HtmlParser
{
    public class CrawlerParser : INewsSourceParser
    {

        public List<Article> Parse(NewsSource source)
        {
            List<Article> result = new List<Article>();
            if (source.Type != NewsSourceType.CrawlingStartPage)
            throw new ArgumentException("This parser can only be used to parse CrawlingStartPage news sources");
            string html = ExtractHtml(source.Url);
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(html);
            UriBuilder builder = new UriBuilder(source.Url);
            builder.Path = String.Empty;
            Uri baseUri = builder.Uri;

            foreach (HtmlNode link in document.DocumentNode.SelectNodes("//a[@href]"))
            {
                HtmlAttribute att = link.Attributes["href"];
                Uri fullUri = new Uri(baseUri, att.Value);
                if (Regex.Match(fullUri.AbsoluteUri, source.ChildLinksMatchingRegularExpression).Success)
                {  
                    IEnumerable<Article> sameArticle = from a in result where a.Url == fullUri.AbsoluteUri select a;
                    
                    //the same link may be found many times in the same page
                    if (sameArticle.Count() != 0)
                        continue;

                    Article newArticle = new Article();
                    newArticle.Url = fullUri.AbsoluteUri;
                    result.Add(newArticle);
                }
            }
            return result;
        }
        private string ExtractHtml(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0";
            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            return reader.ReadToEnd();
        }
    }
}
