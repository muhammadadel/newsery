﻿using Newsery.CassandraDataAccessLayer;
using Newsery.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Newsery.Backoffice
{
    /// <summary>
    /// Interaction logic for NewsPapersControl.xaml
    /// </summary>
    public partial class NewsPapersControl : UserControl
    {
        public NewsPapersControl()
        {
            InitializeComponent();
           
           
        }

        private void LoadNewsPapers(string countryName)
        {
             grdNewsPapers.ItemsSource= NewsSroucesManager.GetNewsPapersByCountryName(countryName);
        }

        private void LoadCountries()
        {
            List<Country> countriesList = NewsSroucesManager.GetCountries();
            foreach (Country c in countriesList)
            {
                comboCountries.Items.Add(c.Name);
                comboCountriesFilter.Items.Add(c.Name);
            }
            comboCountries.SelectedIndex = 0;
            comboCountriesFilter.SelectedIndex = 0;
            
        }

        private void comboCountriesFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadNewsPapers(comboCountriesFilter.SelectedItem.ToString());
        }

        private void btnAddNewNewsPaper_Click(object sender, RoutedEventArgs e)
        {
            Newspaper newNewspaper = new Newspaper();
            newNewspaper.Country = comboCountries.SelectedItem.ToString();
            newNewspaper.Name = txtNewNewsPaperName.Text;
            NewsSroucesManager.AddNewNewspaper(newNewspaper);
            LoadNewsPapers(comboCountriesFilter.SelectedItem.ToString());
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Newspaper selectedNewspaper = (Newspaper)grdNewsPapers.SelectedItem;
            if (selectedNewspaper == null)
                return;
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete selected newspaper ?", "Are you sure", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result != MessageBoxResult.Yes)
                return;
            NewsSroucesManager.DeleteNewsPaper(selectedNewspaper);
            LoadNewsPapers(comboCountriesFilter.SelectedItem.ToString());
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!countriesLoaded)
            {
                LoadCountries();
                countriesLoaded = true;
            }
        }
        private bool countriesLoaded = false;
    }
}
