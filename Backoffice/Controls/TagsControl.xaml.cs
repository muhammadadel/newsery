﻿using Newsery.CassandraDataAccessLayer;
using Newsery.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Newsery.Backoffice
{
    /// <summary>
    /// Interaction logic for TagsControl.xaml
    /// </summary>
    public partial class TagsControl : UserControl
    {
        
        public TagsControl()
        {
            InitializeComponent();
            
        }

        private void LoadCountries()
        {
            comboCountries.Items.Clear();
            comboCountriesFilter.Items.Clear();
             List<Country> countriesList = NewsSroucesManager.GetCountries();
            foreach (Country c in countriesList)
            {
                comboCountries.Items.Add(c.Name);
                comboCountriesFilter.Items.Add(c.Name);
            }
            comboCountries.SelectedIndex = 0;
            comboCountriesFilter.SelectedIndex = 0;
        }

        private void comboCountriesFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboCountriesFilter.SelectedItem !=null)
                LoadTagsInDatagrid(comboCountriesFilter.SelectedItem.ToString());
        }
        private void LoadTagsInDatagrid(string countryName)
        {
            grdTags.ItemsSource = NewsSroucesManager.GetTagsByCountryName(countryName);
        }

        private void btnAddNewTag_Click(object sender, RoutedEventArgs e)
        {
            Tag newTag = new Tag();
            newTag.Country = comboCountries.SelectedItem.ToString();
            newTag.Value = txtTagValue.Text;
            newTag.Order = uint.Parse(txtOrder.Text);
            NewsSroucesManager.AddNewTag(newTag);
            LoadTagsInDatagrid(comboCountriesFilter.SelectedItem.ToString());
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Tag selectedTag = (Tag)grdTags.SelectedItem;
            if (selectedTag == null)
                return;
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete selected tag ?", "Are you sure", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result != MessageBoxResult.Yes)
                return;
            NewsSroucesManager.DeleteTag(selectedTag);
            LoadTagsInDatagrid(comboCountriesFilter.SelectedItem.ToString());
            
        }



        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            LoadCountries();
        }

      

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Tag selectedTag = (Tag)grdTags.SelectedItem;
            if (selectedTag == null)
                return;
            EditTagWindow editWidnow = new EditTagWindow(selectedTag);
            editWidnow.ShowDialog();
            LoadTagsInDatagrid(comboCountriesFilter.SelectedItem.ToString());
        }

      
    }
}
