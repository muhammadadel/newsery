﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newsery.DataTypes;

namespace Newsery.Website
{
    /// <summary>
    /// A class to hold extension methods for Story class
    /// </summary>
    public static  class StoryExtensions
    {
        //the total score of the sotry is the score decreasing exponentially with time
        public static double GetSlidingExpirationScore(this Story story)
        {
            var timeDifference = DateTime.UtcNow-story.Date;
            float hoursPassed = timeDifference.Hours + ((float)timeDifference.Minutes/60);
            double result =story.Score*(Math.Pow(Math.E,-hoursPassed/5));
            return result;
        }
        }
    }
