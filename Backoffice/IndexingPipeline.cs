﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;
using Newsery.TextSearchDAL;
using System.Collections.Concurrent;
using Newsery.HtmlParser;
namespace Newsery.Backoffice
{
    class IndexingPipeline
    {
        
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="article"></param>
        /// <returns>true if the article was indexed, false if it was already indexed before</returns>
        public bool Index(Article article)
        {
            if (IndexerInstance != null)
                Pool.ReturnIndexer(IndexerInstance);
            if (article.NewsSrouceCountry == "US")
            {
                IndexerInstance = Pool.GetIndexer("US");
            }
            else
            {
                IndexerInstance = Pool.GetIndexer("Egypt");
            }
            if (IndexerInstance.ArticleExists(article.Url))
                return false;
            bool parsingResult = ExtractArticleContent(article);
            if (parsingResult)
            {
                if (IndexerInstance.IndexArticle(article))
                    return true;
            }
            return false;
        }

        private bool ExtractArticleContent(Article a)
        {
            IHtmlArticleContentExtractorInterface contentExtractor = ArticleContentExtractorResolver.GetExtractor(a);
            bool parsingResult = contentExtractor.Parse(a);
            return parsingResult;
        }
         ~IndexingPipeline()
        {
            if (IndexerInstance != null)
            {
                Pool.ReturnIndexer(IndexerInstance);
            }
        }
        private Indexer IndexerInstance;
        
    }
}
