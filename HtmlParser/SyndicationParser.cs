﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Xml;
//using nJupiter.Web.Syndication;
using Newsery.DataTypes;
using QDFeedParser;

namespace Newsery.HtmlParser
{
    /// <summary>
    /// A Website parser that parses the a website through RSS links
    /// </summary>
    public class SyndicationParser:INewsSourceParser
    {
        
      

        public List<Article> Parse(NewsSource source)
        {
            if (source.Type != NewsSourceType.Feed)
                throw new ArgumentException("Only NewsSource of Type Feed can by parsed using this parser");

            List<Article> articles = new List<Article>();
            HttpFeedFactory NewsSourceFactory = new HttpFeedFactory();
            IFeed NewsSource = NewsSourceFactory.CreateFeed(new Uri(source.Url));

            foreach (IFeedItem syndicationItem in NewsSource.Items)
            {
                if ((DateTime.Now - syndicationItem.DatePublished).TotalDays > 1)
                    //the news is too old or there is something wrong
                    continue;
                Article article = new Article();
                article.Date = syndicationItem.DatePublished;
                article.Url = syndicationItem.Link;
                article.Title = syndicationItem.Title;
                articles.Add(article);

            }
            return articles;
        }
   
        /// <summary>
        /// Gets Absolute Urls of Syndication Links in The page. Most news sites have a single page that contain different syndication links for sections on the site. 
        /// </summary>
        /// <param name="startUrl">Absolute URL for Website's Main syndication URL or a page that contain all the site's Syndiction Urls</param>
        /// <returns></returns>
        List<string> getSyndicationUrls(string startUrl)
        {
            List<string> urls = new List<string>();
            if (isSyndicationURL(startUrl))
            {
                urls.Add(startUrl);
                return urls;
            }
            object _lock = new object();
            string html = ExtractHtml(startUrl);
            HtmlDocument document = new HtmlDocument();
            document.OptionFixNestedTags = true;
            document.LoadHtml(html);
            HtmlNodeCollection links =  document.DocumentNode.SelectNodes("//a");

            //run in parallel since we will send a request for every link
            ParallelOptions options=new ParallelOptions();
            options.MaxDegreeOfParallelism=1;
            System.Threading.Tasks.Parallel.ForEach(links,options, rssNode =>
            {
                if (!rssNode.Attributes.Contains("href"))
                    return;
                string url = rssNode.Attributes["href"].Value;
                string absoluteURL = getAbsoluteUrl(url);
                if (isSyndicationURL(absoluteURL))
                    lock (_lock)
                    {
                        urls.Add(absoluteURL);
                    }
            });
            return urls;
        }

        private string getAbsoluteUrl(string url)
        {
            Uri uri = new Uri(url, UriKind.RelativeOrAbsolute);
            if (uri.IsAbsoluteUri)
                return url;
            else
            {

                uri = new Uri(_BaseUri, url);
                url = uri.ToString();
                return url;
            }
       
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        /// <remarks>This can be done in a better way using the Syndication classes of .NET 4. for example, this code currently checks only for RSS, not ATOM</remarks>
        private bool isSyndicationURL(string url)
        {
            string html;
            try
            {
                html = ExtractHtml(url);
            }
            catch (Exception  )
            {
                return false;
            }
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(html);
            HtmlNodeCollection rssNodes = document.DocumentNode.SelectNodes("//rss");
            if (rssNodes != null && rssNodes.Count == 1)
                return true;
            HtmlNodeCollection NewsSourceNodes = document.DocumentNode.SelectNodes("//NewsSource");
            if (NewsSourceNodes != null && NewsSourceNodes.Count == 1)
                return true;
            return false;
        }

        private  string ExtractHtml(string url)
        {
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            return reader.ReadToEnd();
        }
        
        private Uri _BaseUri;
        
    }
}
