﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newsery.DataTypes;
using System.Collections.ObjectModel;
using Newsery.CassandraDataAccessLayer;

namespace Newsery.Backoffice
{
    /// <summary>
    /// Interaction logic for CountriesControl.xaml
    /// </summary>
    public partial class CountriesControl : UserControl
    {
       
        public CountriesControl()
        {
            InitializeComponent();
           
        }

     

        private void LoadCountriesInGrid()
        {
            List<Country> countriesList = NewsSroucesManager.GetCountries();
            
            grdCountries.ItemsSource = countriesList;
           
            
        }

        private void btnAddNewCountry_Click(object sender, RoutedEventArgs e)
        {
            Country newCountry = new Country();
            newCountry.Name = txtNewCountryName.Text;
            NewsSroucesManager.AddNewCountry(newCountry);
            LoadCountriesInGrid();
            txtNewCountryName.Text = "";
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Country selectedCountry = (Country)grdCountries.SelectedItem;
            if (selectedCountry == null)
                return;
            MessageBoxResult result= MessageBox.Show("Are you sure you want to delete selected country ?","Are you sure",MessageBoxButton.YesNo,MessageBoxImage.Warning);
            if (result != MessageBoxResult.Yes)
                return;
            NewsSroucesManager.DeleteCountry(selectedCountry);
            LoadCountriesInGrid();

        }

      

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            LoadCountriesInGrid();
        }
    }
}
