﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newsery.CassandraDataAccessLayer;


namespace Newsery.Backoffice
{
    /// <summary>
    /// Interaction logic for CassandraQueryControl.xaml
    /// </summary>
    public partial class CassandraQueryControl : UserControl
    {
        public CassandraQueryControl()
        {
            InitializeComponent();
        }

        private void btnExecute_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtOutput.Text = "";
                List<Dictionary<string, object>> result = TypeLessQueryHandler.ExecuteQuery(txtQuery.Text);
                StringBuilder outputText = new StringBuilder();
                foreach (Dictionary<string, object> d in result)
                {
                    foreach (string key in d.Keys)
                    {
                        outputText.Append(key);
                        outputText.Append(" : ");
                        outputText.Append(d[key].ToString());
                        outputText.Append("\n");
                    }
                    outputText.Append("\n");
                    outputText.Append("\n");
                    outputText.Append("\n");
                    outputText.Append("\n");
                }
                txtOutput.Text = outputText.ToString();

            }
            catch (Exception ex)
            {
                txtOutput.Text = ex.StackTrace;
            }
            
        }
    }
}
