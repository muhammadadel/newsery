﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Http;
using Newsery.DataTypes;
using Newsery.CassandraDataAccessLayer;

namespace Newsery.Website
{
   
    public class SessionManager
    {
        #region Session and Application
        private static HttpSessionState Session
        {
            get
            {
                return HttpContext.Current.Session;
            }
        }
        private static HttpApplicationState Application
        {
            get
            {
                return HttpContext.Current.Application;
            }
        }

        private static string GetExcludedNewspapersByCountryKey(string country)
        {
            return country + "_ExcludedNewspapers";
        }
        private static string GetExcludedTagsByCountryKey(string country)
        {
            return country + "_ExcludedTags";
        }
    #endregion
        /// <summary>
        /// Load User preferences from database into session object
        /// </summary>
        private static void LoadUserPereferencesFromDatabase()
        {
            User loggedInUser =(User)HttpContext.Current.Session["User"];
            if (loggedInUser == null)
                throw new ApplicationException("User not logged in");

            Tag[] unSubscribedTags = GetExcludedTags();
            HttpContext.Current.Session["UnSubscribedTags"] = unSubscribedTags;
            List<string> countries = Application["Countries"] as List<string>;
            //no need to store the whole tag value and no need to filter them per country every time the user requires them, we only store the text value of the tag and we group them per country
            foreach (string country in countries)
            {
                string key = GetExcludedTagsByCountryKey(country);
                string[] countryUnsubscribedTags = (from Tag t in unSubscribedTags where t.Country == country select t.Value).ToArray();
                Session[key] = countryUnsubscribedTags;
            }
            Newspaper[] excludedNewspapers = GetExcludedNewspapers();
            foreach (string country in countries)
            {
                string key = GetExcludedNewspapersByCountryKey(country);
                string[] countryUnsubscribedNewspapers = (from Newspaper n in excludedNewspapers where n.Country == country select n.Name).ToArray();
                Session[key] = countryUnsubscribedNewspapers;
            }
        }
        public static void SetLoggedInUser(User loggedInUser)
        {
            Session["User"] = loggedInUser;
            NewsManager.ClearCache();
            LoadUserPereferencesFromDatabase();

        }
        public static void ClearLoggedInUser()
        {
            Session.RemoveAll();
            Session.Abandon();
            NewsManager.ClearCache();
        }
        internal static Newspaper[] GetExcludedNewspapers()
        {
            User loggedInUser = (User)HttpContext.Current.Session["User"];
            if (loggedInUser == null)
                throw new ApplicationException("User not logged in");

            Newspaper[] excludedNewspapers = UsersManager.GetExcludedNewspapers(loggedInUser.Email);
            return excludedNewspapers;
        }
       
        private static string[] GetExcludedNewsPapersByCountry(string country)
        {
            string key = GetExcludedNewspapersByCountryKey(country);
            return (string[])Session[key];
        }
        #region tags
       
        internal static Tag[] GetExcludedTags()
        {
            User loggedInUser = (User)HttpContext.Current.Session["User"];
            if (loggedInUser == null)
                throw new ApplicationException("User not logged in");

            Tag[] unSubscribedTags = UsersManager.GetUnSubscribedTags(loggedInUser.Email);
            return unSubscribedTags;
        }
        public static List<string> GetTagsByCountry(string currentCountry)
        {
            string tagsKey = currentCountry + "_Tags";
            return (List<string>)Application[tagsKey];
        }
        private static string[] GetSubscribedTagsByCountry(string country)
        {
            string key = GetExcludedTagsByCountryKey(country);
            return (string[])Session[key];
        }
        internal static void SetUserExcludedTags(Tag[] excludedTags)
        {
            User loggedInUser = (User)HttpContext.Current.Session["User"];
            if (loggedInUser == null)
                throw new ApplicationException("User not logged in");

            UsersManager.SetUserExcludedTags(loggedInUser.Email, excludedTags);
            NewsManager.ClearCache();
        }
        #endregion

        public static string GetCurrentCountry()
        {
            return Session["Country"] as string;
        }
        public static void SetCurrentCountry(string country)
        {
            Session["Country"] = country;
        }


        internal static void SetUserExcludedNewspapers(Newspaper[] excludedNewspapers)
        {
            User loggedInUser = (User)HttpContext.Current.Session["User"];
            if (loggedInUser == null)
                throw new ApplicationException("User not logged in");

            UsersManager.SetUserExcludedNewspapers(loggedInUser.Email, excludedNewspapers);
            NewsManager.ClearCache();
        }


        public static User GetLoggedInUser()
        {
            return (User)Session["User"];
        }
        
    }
}