﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Newsery.TextSearchDAL;
using Newsery.CassandraDataAccessLayer;
using Newsery.DataTypes;


namespace Newsery.Website
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            SetDisplayModes();
            LoadData();
        }

        private void SetDisplayModes()
        {
            
            DisplayModeProvider.Instance.Modes.Insert(0,new DefaultDisplayMode("mobile")
            {
                ContextCondition=(c=>c.Request.UserAgent.IndexOf("ipad",StringComparison.OrdinalIgnoreCase) >= 0 ||
                    c.Request.UserAgent.IndexOf("android",StringComparison.OrdinalIgnoreCase) >= 0 ||
                    c.Request.UserAgent.IndexOf("iphone",StringComparison.OrdinalIgnoreCase) >= 0   )
            }
            );
            DisplayModeProvider.Instance.Modes.Insert(0, new DefaultDisplayMode("app")
            {
                ContextCondition = (c => c.Request.UserAgent.IndexOf("newsery", StringComparison.OrdinalIgnoreCase) >= 0 )
            }
             );
        }

      
        protected void Session_Start()
        {
            if (Session["Country"] == null)
            {
                //currently we have one country only. In the future, we should query a library to get the country based on the ip address of the user
                Session["Country"] = "مصر";
            }
            if (Request.Cookies["NewseryLogin"] != null)
            {
                Session["RequestedUri"] = Request.Url.AbsoluteUri;
                UrlHelper url = new UrlHelper(Request.RequestContext);
                Response.Redirect(url.Action("LoginFromCookie", "Login", null, "https"),true);
            }
        }
        private void LoadData()
        {
            LoadCountries();
            LoadTags();
            LoadNewsPapers();  
            LoadStoriesInCache();
        }

        private void LoadCountries()
        {
            List<Country> Countries = NewsSroucesManager.GetCountries();
            List<string> countriesString = new List<string>();
            foreach (Country c in Countries)
            {
                countriesString.Add(c.Name);
            }
            Application["Countries"] = countriesString;
        }

        private void LoadTags()
        {
            List<string> countries =(List<string>) Application["Countries"];
            foreach (string country in countries)
            {
                string tagsKey = country + "_Tags";
                List<Tag> tags = NewsSroucesManager.GetTagsByCountryName(country).OrderBy(t => t.Order).ToList() ;
                List<string> stringTags = new List<string>();
                foreach (Tag t in tags)
                {
                    stringTags.Add(t.Value);
                }
                Application[tagsKey] = stringTags;
            }
            
        }

        private void LoadNewsPapers()
        {
            List<string> countries = (List<string>)Application["Countries"];
            foreach (string country in countries)
            {
                string newspapersKey = country + "_Newspapers";
                List<Newspaper> newspapers = NewsSroucesManager.GetNewsPapersByCountryName(country);
                List<string> stringNewspapers = new List<string>();
                foreach (Newspaper n in newspapers)
                {
                    stringNewspapers.Add(n.Name);
                }
                Application[newspapersKey] = stringNewspapers;
            }
        }

        private void LoadStoriesInCache()
        {
            StoriesManager stroiesManager = new StoriesManager("egypt");
            List<Story> stories = stroiesManager.GetStories(1500);
            var rejectedStories = from Story s in stories where s.Tags.Count == 0 select s;
            stories =stories.Except(rejectedStories).ToList();
            SortStoriesSlidingExpirationScore(ref stories);
            HttpContext.Current.Cache.Insert("EgyptStories", stories, null, DateTime.Now.AddMinutes(1), Cache.NoSlidingExpiration, CacheItemUpdateCallback);
        }
        private void CacheItemUpdateCallback(string key, CacheItemUpdateReason reason, out object value, out CacheDependency dependency, out DateTime exipriation, out TimeSpan slidingExpiration)
        {
            if (key == "EgyptStories")
            {
                try
                {
                    StoriesManager stroiesManager = new StoriesManager("egypt");
                    List<Story> stories = stroiesManager.GetStories(1500);
                    var rejectedStories = from Story s in stories where s.Tags.Count == 0 select s;
                    stories = stories.Except(rejectedStories).ToList();
                    SortStoriesSlidingExpirationScore(ref stories);
                    value = stories;
                    slidingExpiration = Cache.NoSlidingExpiration;
                    exipriation = DateTime.Now.AddMinutes(2);
                    dependency = null;

                }
                catch
                {
                    //create an empty list for now and try again five seconds later
                    List<Story> emptyStories = new List<Story>();
                    value = emptyStories;
                    slidingExpiration = Cache.NoSlidingExpiration;
                    exipriation = DateTime.Now.AddSeconds(5);
                    dependency = null;
                }
            }
            else
            {
                value = null;
                exipriation = Cache.NoAbsoluteExpiration;
                slidingExpiration = Cache.NoSlidingExpiration;
                dependency = null;
            }
            
        }

        private void  SortStoriesSlidingExpirationScore(ref List<Story> stories)
        {
            stories = (from s in stories orderby s.GetSlidingExpirationScore() descending select s).ToList(); 
        }

    }
}