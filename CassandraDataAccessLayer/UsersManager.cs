﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;
using CassandraSharp;
using CassandraSharp.CQLPoco;
using CassandraSharp.CQLPropertyBag;
using CassandraSharp.CQLCommand;
using System.Web.Script.Serialization;

namespace Newsery.CassandraDataAccessLayer
{
    public class UsersManager
    {
        public static User GetUserByEmail(string email)
        {
            ICluster cluster = CassandraClient.Cluster;
            string selectUserCommand = "Select * from Newsery.Users where Email=? ";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            PropertyBag parametersBag = new PropertyBag();
            parametersBag["Email"] = email;
            var preparedQuery = command.Prepare(selectUserCommand);
            var users = preparedQuery.Execute(parametersBag).AsFuture().Result;
            if (users.Count == 0)
                return null;
            PropertyBag userBag= users.First();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            User requestedUser = serializer.Deserialize<User>((string)userBag["Data"]);
            return requestedUser;
        }
        public static void DeleteUserByEmail(string email)
        {
            User TempUser = new User();
            TempUser.Email = email;
            ICluster cluster = CassandraClient.Cluster;
            string selectUserCommand = "Delete from Newsery.Users where Email=? ";
            ICqlCommand command = cluster.CreatePocoCommand();
            var preparedQuery = command.Prepare(selectUserCommand);
            preparedQuery.Execute(TempUser).AsFuture().Wait();
            
        }
        public static Newspaper[] GetExcludedNewspapers(string email)
        {
            string selectExcludedNewspapersCommand = "Select Data from Newsery.ExcludedNewspapers where Email=?";
            IPropertyBagCommand command = CassandraClient.Cluster.CreatePropertyBagCommand();
            var preparedQuery = command.Prepare(selectExcludedNewspapersCommand);
            PropertyBag parametersBag = new PropertyBag();
            parametersBag["Email"] = email;
            IList<PropertyBag> resultBags = preparedQuery.Execute(parametersBag).AsFuture().Result;
            Newspaper[] unsubscribedNewspapers = null;
            if (resultBags.Count == 0)
                unsubscribedNewspapers = new Newspaper[0];
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                unsubscribedNewspapers = serializer.Deserialize<Newspaper[]>((string)resultBags.First()["Data"]);
            }
            return unsubscribedNewspapers;
        }
        public static Tag[] GetUnSubscribedTags(string email)
        {
            string selectTagsCommand = "Select Data from Newsery.ExcludedTags where Email=?";
            IPropertyBagCommand command = CassandraClient.Cluster.CreatePropertyBagCommand() ;
            var preparedQuery=command.Prepare(selectTagsCommand);
            PropertyBag parametersBag = new PropertyBag();
            parametersBag["Email"] = email;
            IList<PropertyBag> unsubscribedTagsBag= preparedQuery.Execute(parametersBag).AsFuture().Result;
           
            Tag[] unsubscribedTags=null;
            if (unsubscribedTagsBag.Count > 0)
            { 
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                unsubscribedTags = serializer.Deserialize<Tag[]>((string)unsubscribedTagsBag.First()["Data"]);
            }
            else
                unsubscribedTags = new Tag[0];
            return unsubscribedTags;
        }

        public static void AddUser(User newUser)
        {
            string insertUserCommand = "Insert Into Newsery.Users (email,Data) values(?,?) ";
            IPropertyBagCommand command = CassandraClient.Cluster.CreatePropertyBagCommand();
            var preparedQuery = command.Prepare(insertUserCommand);
            PropertyBag parametersBag = new PropertyBag();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            parametersBag["Email"] = newUser.Email;
            parametersBag["Data"] = serializer.Serialize(newUser);
            preparedQuery.Execute(parametersBag).AsFuture().Wait();
        }

        public static void SetUserExcludedTags(string email, Tag[] excludedTags)
        {
            string selectTagsCommand = "Update  Newsery.ExcludedTags set Data =? where Email=?";
            IPropertyBagCommand command = CassandraClient.Cluster.CreatePropertyBagCommand();
            var preparedQuery = command.Prepare(selectTagsCommand);
            PropertyBag parametersBag = new PropertyBag();
            parametersBag["Email"] = email;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            parametersBag["Data"] = serializer.Serialize(excludedTags);
            preparedQuery.Execute(parametersBag).AsFuture().Wait();
        }

        public static void SetUserExcludedNewspapers(string email, Newspaper[] excludedNewspapers)
        {
            string selectTagsCommand = "Update  Newsery.ExcludedNewspapers set Data =? where Email=?";
            IPropertyBagCommand command = CassandraClient.Cluster.CreatePropertyBagCommand();
            var preparedQuery = command.Prepare(selectTagsCommand);
            PropertyBag parametersBag = new PropertyBag();
            parametersBag["Email"] = email;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            parametersBag["Data"] = serializer.Serialize(excludedNewspapers);
            preparedQuery.Execute(parametersBag).AsFuture().Wait();
        }
    }
}
