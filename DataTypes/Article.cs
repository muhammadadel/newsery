﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
namespace Newsery.DataTypes
{
    [ElasticType(IdProperty = "id")]
    public  class Article
    {
        public  Article()
        {
            Tags = new List<string>();
            TagComputed = false;
        }
        
        public string Content { get; set; }
        [ElasticProperty( Index = FieldIndexOption.not_analyzed)]
        public string Url { get; set; }
        [ElasticProperty (Boost=3.0)]
        public string Title { get; set; }
         [ElasticProperty(Index = FieldIndexOption.not_analyzed)]
        public string MainImageUrl { get; set; }
          [ElasticProperty(Index = FieldIndexOption.not_analyzed)]
        public List<string> Tags{get;set;}
        public DateTime Date { get; set; }
         [ElasticProperty(Index = FieldIndexOption.not_analyzed)]
        public string NewsPaper { get; set; }
        [ElasticProperty(Boost = 1.5)]
        public string Summary { get; set; }
        [ElasticProperty(Index = FieldIndexOption.not_analyzed)]
        public string NewsSrouceCountry { get; set; }
        public int Score { get; set; }

        [ElasticProperty(Index = FieldIndexOption.not_analyzed)]
        public string Id
        {
            get
            {
                if (_Id == null || _Id == string.Empty)
                {
                    _Id = Guid.NewGuid().ToString();
                }
                return _Id;
            }
            set
            {
                _Id = value;
            }

        }
        private string _Id;
        public bool TagComputed { get; set; }

        
    }
}
