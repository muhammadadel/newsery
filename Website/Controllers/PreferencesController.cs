﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newsery.CassandraDataAccessLayer;
using Newsery.DataTypes;
using Newsery.Website;
namespace Newsery.Website.Controllers
{
    public class PreferencesController : Controller
    {
        //
        // GET: /Tags/

        public ActionResult Index()
        {
            PreferencesModel model = new PreferencesModel();
            model.SelectedTags = new List<SelectedTag>();
            Tag[] excludedTags = SessionManager.GetExcludedTags();
            List<Tag> allTags = NewsSroucesManager.GetTags();
            //display the tags ordered
            allTags = allTags.OrderBy(t => t.Order).ToList();
            foreach (Tag tag in allTags)
            {
                var v = from Tag t in excludedTags where t.Id == tag.Id select t;
                SelectedTag selected=new SelectedTag();
                selected.Value=tag.Value;
                selected.Id = tag.Id.ToString();
                selected.Country = tag.Country;
                if (v.Count<Tag>()> 0)
                selected.IsSelected = false;
                else
                    selected.IsSelected = true;
                model.SelectedTags.Add(selected);
            }
            model.SelectedNewspapers = new List<SelectedNewspaper>();
            Newspaper[] excludedNewspapers = SessionManager.GetExcludedNewspapers();
            List<Newspaper> allNewspapers = NewsSroucesManager.GetNewspapers();
            foreach (Newspaper newspaper in allNewspapers)
            {
                var v = from Newspaper n in excludedNewspapers where n.Id == newspaper.Id select n;
                SelectedNewspaper selected = new SelectedNewspaper();
                selected.Id = newspaper.Id.ToString();
                selected.Name = newspaper.Name;
                selected.Country = newspaper.Country;
                if (v.Count<Newspaper>() > 0)
                    selected.IsSelected = false;
                else
                    selected.IsSelected = true;
                model.SelectedNewspapers.Add(selected);
            }
            if ((string)Session["Country"] == "مصر")
                return View("Arabic",model);
            else
                throw new ApplicationException("Unsupported Country");
        }
        [HttpPost]
        public ActionResult Index(PreferencesModel model)
        {
            string[] excluededIds = (from SelectedTag selected in model.SelectedTags where selected.IsSelected == false select selected.Id).ToArray();
            List<Tag> allTags = NewsSroucesManager.GetTags();
            Tag[] excludedTags = (from Tag t in allTags where excluededIds.Contains(t.Id.ToString()) select t).ToArray();
            SessionManager.SetUserExcludedTags(excludedTags);
            
            string []excludedNewspapersIds = (from SelectedNewspaper selected in model.SelectedNewspapers where selected.IsSelected == false select selected.Id).ToArray();
            List<Newspaper>allNewspapers = NewsSroucesManager.GetNewspapers();
            Newspaper[]excludedNewspapers = (from Newspaper n in allNewspapers where excludedNewspapersIds.Contains(n.Id.ToString()) select n).ToArray();
            SessionManager.SetUserExcludedNewspapers(excludedNewspapers);
            
            return Content("تم الحفظ بنجاح");
        }

    }
}
