﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NBoilerpipe.Extractors;
using Newsery.DataTypes;

namespace Newsery.HtmlParser
{
    public class NBoilerpipeHTMLArticleContentExtractor:IHtmlArticleContentExtractorInterface
    {
        public Article Parse(string url)
        {
            string html = ExtractHtml(url);
            Article result = new Article();
            result.Content = ArticleExtractor.GetInstance().GetText(html);
            return result;
        }
        
  
        private string ExtractHtml(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
             return reader.ReadToEnd();
        }



        public bool Parse(Article article)
        {
            string html = ExtractHtml(article.Url);
            string content = ArticleExtractor.GetInstance().GetText(html);
            string[] statements = content.Split(new char[] { '\n' });
            //the first statement is the title. Remove it
            content = content.Replace(statements[0], "");
            article.Content = content;
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(html);
            if (article.Title.Contains("�"))
            {
                //article title could not be parsed. get it from the document
                article.Title = document.DocumentNode.SelectSingleNode("//title").InnerText;
               article.Title = article.Title.Trim();
                article.Title=article.Title.Replace(article.NewsPaper, "");
                article.Title = statements[0];
            }
            DefaultArticleSummarizer summarizer = new DefaultArticleSummarizer();
            article.Summary = summarizer.Summarize(article.Content);
            return true;
        }
      
    }
}
