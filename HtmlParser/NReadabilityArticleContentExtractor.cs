﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NReadability;
using HtmlAgilityPack;
using NBoilerpipe.Extractors;
using Newsery.DataTypes;
using System.Net;
using System.IO;

namespace Newsery.HtmlParser
{
    public class NReadabilityArticleContentExtractor:IHtmlArticleContentExtractorInterface
    {
        

        public bool Parse(Article article)
        {
            NReadabilityWebTranscoder WebTranscoder = new NReadabilityWebTranscoder();
            DomSerializationParams serializationParameters = DomSerializationParams.CreateDefault();
            serializationParameters.DontIncludeDocTypeMetaElement = true;
            
            serializationParameters.DontIncludeMobileSpecificMetaElements = true;
            WebTranscodingResult transcodingResult;

            try
            {
                WebTranscodingInput input = new WebTranscodingInput(article.Url);
                input.DomSerializationParams = serializationParameters;
                transcodingResult = WebTranscoder.Transcode(input);


                if (!transcodingResult.ContentExtracted)
                    return false;

            }
            catch (Exception)
            {
                return false;
            }


            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(transcodingResult.ExtractedContent);            
            string content = ArticleExtractor.GetInstance().GetText(transcodingResult.ExtractedContent);
            if (content == string.Empty)
                return false;
             string[] statements = content.Split(new char[] { '\n'});
            //the first statement is the title once again. Remove it
            content = content.Replace(statements[0], "");
            article.Content = content;
            if (article.Title==null || article.Title.Contains("�") || article.Title=="")
                //article title could not be parsed. Extract it from content could not be parsed
                article.Title = statements[0];

            DefaultArticleSummarizer summarizer = new DefaultArticleSummarizer();
            article.Summary = summarizer.Summarize(article.Content);

            HtmlNode imageNode = document.DocumentNode.SelectSingleNode("//meta[@property='og:image']");
            if (imageNode != null && imageNode.Attributes["content"]!=null &&article.NewsPaper!="الشعب")
                article.MainImageUrl = imageNode.Attributes["content"].Value;
            
            return true;

        }

    }
}
