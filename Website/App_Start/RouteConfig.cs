﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Newsery.Website
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Login", "login/{action}", new { controller = "login" });
            routes.MapRoute("Preferences","preferences",new {controller="Preferences", action="Index"});
            routes.MapRoute("CountryAll", "{country}", new { controller = "Home", action = "All" });
            routes.MapRoute("PaginatedAll", "{country}/next/{lastStoryId}", new { controller = "Home", action = "All" ,lastStoryId=UrlParameter.Optional}); 
            routes.MapRoute("CountryTag", "{country}/{tag}", new { controller = "Home", action = "Tag" });
            routes.MapRoute("PaginatedTaged", "{country}/{Tag}/next/{lastStoryId}", new { controller = "Home", action = "Tag", lastStoryId = UrlParameter.Optional });
            routes.MapRoute("Story", "story/{country}/{storyId}", new { controller ="story", action="index"});
           
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Home", action = "Index" }
            );
          
        }
    }
}