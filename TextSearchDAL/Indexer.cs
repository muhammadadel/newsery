﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;
using Nest;
using System.Configuration;


namespace Newsery.TextSearchDAL
{
    /// <summary>
    /// The class responsible for collecting related articles together, indexing article aggregations and giving them score
    /// </summary>
    public class Indexer:ElasticSearchClient
    {
        private int MinimumAcceptedWordsPerArticle;
        public string IndexName;
        private static UrlCache CachedUrls=new UrlCache(20000);
        public Indexer(string indexName)
        {
            IndexName = indexName;
            _ConnectionSettings.SetDefaultIndex(indexName);
            string minimumAcceptedWords =ConfigurationManager.AppSettings["MinimumAcceptedWordCountPerArticle"];
            MinimumAcceptedWordsPerArticle = Int32.Parse(minimumAcceptedWords);
        }
        public bool IndexArticle(Article article)
        {
            int wordsCount = GetWordCount(article.Content);
            if (wordsCount < MinimumAcceptedWordsPerArticle)
                return false;
            List<IHit<Story>> similarStories = GetSimilarArticleAggregations(article);
            if (article.Tags.Count == 0)
                AutoTagArticle(article, similarStories);
            else
            {
                //tag untagged similar stories
                foreach (IHit<Story> similarStory in similarStories)
                {
                    if (similarStory.Source.Tags.Count == 0 || (similarStory.Source.TagComputed  &&  !article.TagComputed) )
                    {
                        similarStory.Source.Tags = article.Tags;
                        similarStory.Source.TagComputed = false;
                        UpdateStory(similarStory.Source);
                    }
                }
            }
            article.Score = CalculateArticleScore(similarStories);
            Story HighlySimilarArtilces=null;
            //an article is similar to a story if the time difference between them is less than one day and the score of flt query is greater than or equal to 0.4
            foreach (IHit<Story> s in similarStories)
            {
                if ((article.Date - s.Source.Date).Days >= 1)
                    continue;
                    //as the number of articles increase, we raise the threshold
                    if (s.Score<(0.45 + 0.05*s.Source.Articles.Count ))
                        continue;
                    if (s.Source.Articles.Find(a => a.NewsPaper == article.NewsPaper) != null)
                    {
                        //raise the threshold score to take the same article from the same newspaper
                        if (s.Score > 0.9)
                        {
                            HighlySimilarArtilces = s.Source;
                            article.Score = 0;
                        }
                        else
                            continue;
                    }
                    else
                        HighlySimilarArtilces = s.Source;
                
                if (HighlySimilarArtilces != null)
                {
                    UpdateStory(HighlySimilarArtilces, article);
                    return true;
                }
            }

                //there were no articles similar to this article in the database
                CreateNewStory(article);
                CachedUrls.AddKey(article.Url);
            return true;
          
        }

        private void UpdateStory(Story story)
        {
            //updates are not working for a reason I don't know. I have to delete the old story and add a new one
            var deleteResponse = Client.Delete(story);
            IndexParameters parameters = new IndexParameters();
            parameters.TTL = "10d";
            var indexResponse = Client.Index<Story>(story, parameters);
            if (!indexResponse.OK)
                throw new ApplicationException("Error while indexing");
        }

        private void AutoTagArticle(Article article, List<IHit<Story>> similarArticleAggregations)
        {
            Dictionary<string, float> tagsScoreDictionary = new Dictionary<string, float>();
            for (int i = 0; i < similarArticleAggregations.Count; i++)
            {
                
                if (similarArticleAggregations[i].Source.Tags != null && similarArticleAggregations[i].Source.Tags.Count > 0)
                {
                    float score = similarArticleAggregations[i].Source.Score;
                    foreach (string tag in similarArticleAggregations[i].Source.Tags)
                    {
                        if (tagsScoreDictionary.ContainsKey(tag))
                        {
                            float oldScore = tagsScoreDictionary[tag];
                            float newScore = oldScore + score;
                            tagsScoreDictionary[tag] = newScore;
                        }
                        else
                        {
                            tagsScoreDictionary[tag] = score;
                        }
                    }
                }
            }
            if (tagsScoreDictionary.Count == 0)
                return;
            string topScoreTag = tagsScoreDictionary.OrderByDescending(kp => kp.Value).Select(kp => kp.Key).First();
            article.Tags.Add(topScoreTag);
            article.TagComputed = true;
        }
        private int CalculateArticleScore( List<IHit<Story>> similarArticles )
        {
            IEnumerable<IHit<Story>> filteredSimilarArticles = from s in similarArticles where s.Score <0.5 select s;
            int articlesCount=0;
            for (int i = 0; i < filteredSimilarArticles.Count(); i++)
            {
                articlesCount += similarArticles[i].Source.Articles.Count;
            }
            float scoreMultiplier = 1 + articlesCount * 0.005f;
            return (int)(scoreMultiplier*1000);
        }
        private void CreateNewStory(Article article)
        {
            Story story = new Story();
            story.Articles.Add(article);
            story.Score = article.Score;
            story.Tags = article.Tags;  
            story.Date = article.Date;
            IndexParameters parameters = new IndexParameters();
            //create a new story with small time to live. if more articles were added the it could be important and we may increase its ttl
            parameters.TTL = "1d";
            story.TagComputed = article.TagComputed;
            if (!Client.Index<Story>(story, parameters).OK)
                throw new ApplicationException("Error while indexing");
        }

        private void UpdateStory(Story similarArticleAggregation, Article article)
        {
            //updates are not working for a reason I don't know. I have to delete the old article aggregation and add a new one
            var deleteResponse = Client.Delete(similarArticleAggregation);
            similarArticleAggregation.Articles.Add(article);
            if (article.Date<similarArticleAggregation.Date)
                similarArticleAggregation.Date = article.Date;
            //Sometiems more than one article come from one newspaper. add the score once per newspaper.
            List<string> newpapers = new List<string>();
            if (!newpapers.Contains(article.NewsPaper))
            {
                similarArticleAggregation.Score += article.Score;
                newpapers.Add(article.NewsPaper);
            }
            IndexParameters parameters = new IndexParameters();
            parameters.TTL = "10d";
             var indexResponse = Client.Index<Story>(similarArticleAggregation,parameters);
            if (!indexResponse.OK)
                throw new ApplicationException("Error while indexing");
        }
        
        public bool ArticleExists(string articleUrl)
        {
            if (CachedUrls.KeyExists(articleUrl))
                return true;
            var response = Client.Search<Story>(s => s.Query(q => q.Term("articles.url",articleUrl)));
            if (response.Documents.Count()>0)
                return true;
            return false;
        }

        private void IndexArticle(Article article, List<Article> similarArticles)
        {
            if (similarArticles.Count == 0)
                CreateNewStory(article);
        }


        /// <summary>
        /// Get articles that are talking about the same topic of the given article
        /// </summary>
        /// <param name="article"></param>
        /// <returns></returns>
        private  List<IHit<Story>> GetSimilarArticleAggregations(Article article)
        {
            var response = Client.Search<Story>(s => s.Query(
                q => q.FuzzyLikeThis(
                    m => m.LikeText(article.Content).MaxQueryTerms(3000).MinimumSimilarity(0.6).PrefixLength(2)
                    )
                    ).MinScore(0.3).SortDescending("_score").Take(250)  );
            
            return response.DocumentsWithMetaData.ToList();
            
        }

        private int GetWordCount(string text)
        {
            string[] words = text.Split(new char[] { ' ', ',', ';', '.', '!', '"', '(', ')', '?' },StringSplitOptions.RemoveEmptyEntries);
            return words.Count();
        }
    }
}


