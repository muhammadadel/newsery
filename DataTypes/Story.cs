﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace Newsery.DataTypes
{
   [ElasticType (IdProperty="id")]
    public  class Story
    {
       public Story()
       {
           Articles = new List<Article>();
       }
       [ElasticProperty]
       public List<Article> Articles
       {
           get;
           set;

       }

       public DateTime Date
       {
           get;set;
       }
        [ElasticProperty(Index = FieldIndexOption.not_analyzed)]
        public List<string> Tags { get; set; }

       [ElasticProperty(Index = FieldIndexOption.not_analyzed)]
        public string Id
        {
            get
            {
                if (_Id == null || _Id==string.Empty)
                {
                    _Id = Guid.NewGuid().ToString(); 
                }
                return _Id;
            }
            set
            {
                _Id = value;
            }
            
        }
        private string _Id;
        public int Score;
        public string summary { get; set; }
        
        public Story Copy()
        {
            return this.MemberwiseClone() as Story;
        }
        public bool TagComputed
        {
            get;
            set;
        }
        
    }
}
