﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;
using System.Text.RegularExpressions;
namespace Newsery.HtmlParser
{
    public class AljazeeraArabicArticleContentExtractor:IHtmlArticleContentExtractorInterface
    {

        public bool Parse(Article article)
        {
            NBoilerpipeHTMLArticleContentExtractor parser = new NBoilerpipeHTMLArticleContentExtractor();
            bool parsingResult = parser.Parse(article);
            int index = article.Content.IndexOf("شروط الخدمة");
            if (index != -1)
                article.Content = article.Content.Remove(index);
            //article.Title = article.Title.Replace("\n", "");
            //article.Title = article.Title.Replace("\r", "");
            //article.Title = Regex.Replace(article.Title, "الأخبار -.*-", "");
            return parsingResult;
        }
    }
}
