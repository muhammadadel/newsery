﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;
namespace Newsery.HtmlParser
{
    public class ArticleContentExtractorResolver
    {
        public static IHtmlArticleContentExtractorInterface GetExtractor(Article a)
        {
            if (a.Url.Contains("http://arabic.cnn.com"))
                return new CnnArabicArticleContentExtractor();
            if (a.Url.Contains("aljazeera.net"))
                return new AljazeeraArabicArticleContentExtractor();
            if (a.Url.Contains("http://ara.reuters.com"))
                return new ReutersArabicArticleContentExtractor();
            else
                return new NReadabilityArticleContentExtractor();
        }
    }
}
