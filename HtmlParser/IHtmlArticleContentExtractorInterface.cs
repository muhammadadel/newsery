﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;

namespace Newsery.HtmlParser
{
    public interface IHtmlArticleContentExtractorInterface
    {
        bool Parse(Article article);
      
    }
}
