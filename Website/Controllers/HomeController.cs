﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.Web.Mvc;
using Newsery.DataTypes;

namespace Newsery.Website.Controllers
{
    public class HomeController : Controller
    {
        private int _PageSize=20;
        public ActionResult Index()
        {
            //this line to prevent the browser from caching home page, the one which is opened when the user logs in
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            return RedirectToAction("All", new  { country="مصر"});
        }

       
        public ActionResult All(string country, string lastStoryId)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            SessionManager.SetCurrentCountry(country);
            if (lastStoryId == null)
                lastStoryId = "";
            if (Request.IsAjaxRequest())
            {
                return PartialView("ArabicStoryPagePartial", GetPaginatedStories(country,lastStoryId));
            }
            else
            {
                ViewBag.SelectedTag = "All";
                return View("Arabic", GetPaginatedStories(country, lastStoryId));
            }
          
        }
        public ActionResult Tag(string country, string tag, string lastStoryId)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            SessionManager.SetCurrentCountry(country);
            if (lastStoryId == null)
                lastStoryId = "";
            if (Request.IsAjaxRequest())
            {
                return PartialView("ArabicStoryPagePartial", GetPaginatedStories(country,tag, lastStoryId));
            }
            else
            {
                ViewBag.SelectedTag = tag;
                return View("Arabic", GetPaginatedStories(country,tag, lastStoryId));
            }
        } 
        private List<Story> GetPaginatedStories(string country,string lastStoryId)
        {
            List<Story> stories = NewsManager.GetFilteredStories(country).ToList();
            int startIndex;
            if (lastStoryId == "")
                startIndex = 0;
            else
            {
                startIndex=stories.FindIndex(s => s.Id == lastStoryId) + 1;
                
            }
            return stories.Skip(startIndex).Take(_PageSize).ToList();
        }

        private List<Story> GetPaginatedStories(string country,string tag, string lastStoryId)
        {

            List<Story> stories = NewsManager.GetFilteredStories(country,tag).ToList();
            int startIndex;
            if (lastStoryId == "")
                startIndex = 0;
            else
            {
                startIndex = stories.FindIndex(s => s.Id == lastStoryId) + 1;

            }
            return stories.Skip(startIndex).Take(_PageSize).ToList();
        }


    }
}
