﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newsery.DataTypes
{
    public enum NewsSourceType
    {
        //enum values will be stroed in the database. set them to specific values manually to avoid problems when adding or new enum balues
        Feed=102,CrawlingStartPage=304
    }
    public class NewsSource
    {
        public Guid Id
        {
            get
            {
                if (_Id == new Guid())
                {
                    _Id = Guid.NewGuid();
                }
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }
        public NewsSourceType Type
        {
            get;
            set;
        }
        public Guid NewsPaperId { get; set; }
        public string Url { get; set; }
        /// <summary>
        /// This value is used if the news source type is CrawlingStartPage
        /// </summary>
        public string ChildLinksMatchingRegularExpression { get; set; }
        public Guid TagId{get;set;}
        //This value is for display, it is not stored in the database
        public string TagValue { get; set; }
        public string Country { get; set; }
        //This value is for display, it is not stored in the database
        public string NewspaperName { get; set; }
        private Guid _Id;
    }
}
