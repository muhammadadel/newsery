﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;

namespace Newsery.HtmlParser
{
    public class CnnArabicArticleContentExtractor:IHtmlArticleContentExtractorInterface
    {
        public bool Parse(Article article)
        {
            NReadabilityArticleContentExtractor contentExtractor = new NReadabilityArticleContentExtractor();
            bool result = contentExtractor.Parse(article);
            if (result)
            {
                int index = article.Content.IndexOf("ترحب شبكة CNN بالنقاش");
                if(index!=-1)
                    article.Content= article.Content.Remove(index);
            }
            return result;
        }
    }
}
