﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;

namespace Newsery.HtmlParser
{
    public class ReutersNewsSouceParser:INewsSourceParser
    {

        public List<Article> Parse(NewsSource source)
        {
            CrawlerParser parser = new CrawlerParser();
            List<Article> articles = parser.Parse(source);
            foreach (Article a in articles)
                //add query string to get the whole article in single page
                a.Url += "?sp=true";
                return articles;
        }
    }
}
