﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
namespace Newsery.TextSearchDAL
{
    public abstract class ElasticSearchClient
    {
        public ElasticSearchClient()
        {
            Uri newUri = new Uri("http://localhost:9200");
            _ConnectionSettings = new ConnectionSettings(newUri);
            _ConnectionSettings.SetDefaultIndex ( "egypt" );
            _ConnectionSettings.UsePrettyResponses(true);
            
            
            Client = new ElasticClient(_ConnectionSettings);
        
        
        }
        protected ElasticClient Client;
        protected ConnectionSettings _ConnectionSettings;

    }
}
