﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newsery.DataTypes;
using Newsery.CassandraDataAccessLayer;

namespace Newsery.Backoffice
{
    /// <summary>
    /// Interaction logic for NewsSourcesControl.xaml
    /// </summary>
    public partial class NewsSourcesControl : UserControl
    {
        public NewsSourcesControl()
        {
            InitializeComponent();
            FillTypeComboBox();
        }

        private void FillTypeComboBox()
        {
            foreach (var value in Enum.GetValues(typeof(NewsSourceType)))
            {
                string enumName = value.ToString();
                int intValue = (int)value;
                ComboBoxItem newItem = new ComboBoxItem();
                newItem.Tag = intValue;
                newItem.Content = enumName;
                comboType.Items.Add(newItem);
            }
            comboType.SelectedIndex =0;
        }
       

        

        private void LoadCountries()
        {
            comboCountries.Items.Clear();
            comboCountriesFilter.Items.Clear();

            List<Country> countriesList = NewsSroucesManager.GetCountries();
            foreach (Country c in countriesList)
            {
                comboCountries.Items.Add(c.Name);
                comboCountriesFilter.Items.Add(c.Name);
            }
            comboCountries.SelectedIndex = 0;
            comboCountriesFilter.SelectedIndex = 0;
            
        }
        
        
        
        
       

        private void comboCountriesFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboCountriesFilter.SelectedItem == null)
            {
                comboNewspapersFilter.SelectedItem = -1;
                return;
            }
            string selectedCountry = comboCountriesFilter.SelectedItem.ToString();
            List<Newspaper> filteredNewspapers = NewsSroucesManager.GetNewsPapersByCountryName(selectedCountry);
            comboNewspapersFilter.ItemsSource = filteredNewspapers;
        }

        private void comboCountries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboCountries.SelectedItem == null)
            {
                comboNewspapers.SelectedIndex = -1;
                return;
            }
            string selectedCountry = comboCountries.SelectedItem.ToString();
            List<Newspaper> filteredNewspapers = NewsSroucesManager.GetNewsPapersByCountryName(selectedCountry);
            comboNewspapers.ItemsSource = filteredNewspapers;
            if (filteredNewspapers.Count>0)
                comboNewspapers.SelectedIndex = 0;
            List<Tag> filteredTags = NewsSroucesManager.GetTagsByCountryName(selectedCountry);
            comboTags.ItemsSource = filteredTags;
        }

        private void btnAddNewNewsSource_Click(object sender, RoutedEventArgs e)
        {
            NewsSource newNewsSource = new NewsSource();
            if (comboTags.SelectedItem != null)
            {
                Tag selectedTag = comboTags.SelectedItem as Tag;
                newNewsSource.TagId = selectedTag.Id;
                newNewsSource.TagValue = selectedTag.Value;
            }
             newNewsSource.Url = txtUrl.Text;
             ComboBoxItem typeComboItem = (ComboBoxItem)comboType.SelectedItem;
             newNewsSource.Type = (NewsSourceType)typeComboItem.Tag;
             newNewsSource.ChildLinksMatchingRegularExpression = txtRegularExpression.Text;
            newNewsSource.Country = comboCountries.SelectedItem.ToString();
            Newspaper selectedNewspaper = comboNewspapers.SelectedItem as Newspaper;
            newNewsSource.NewsPaperId = selectedNewspaper.Id;
            newNewsSource.NewspaperName = selectedNewspaper.Name;
            NewsSroucesManager.AddNewNewsSource(newNewsSource);
            LoadNewsSources();
            }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            LoadCountries();
        }

        private void comboNewspapersFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadNewsSources();
        }

        private void LoadNewsSources()
        {
            Newspaper selectednewspaper = comboNewspapersFilter.SelectedItem as Newspaper;
            if (selectednewspaper != null)
            {

                List<NewsSource> NewsSources = NewsSroucesManager.GetNewsSourcesByNewspaperId(selectednewspaper.Id);
                List<Tag> tags = (List<Tag>)comboTags.ItemsSource;
                grdNewsSources.ItemsSource = NewsSources;
            }
            else
                grdNewsSources.ItemsSource = null;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result= MessageBox.Show("Are you sure you want to delete this NewsSource?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result != MessageBoxResult.Yes)
                return;

            NewsSource selectedNewsSource = grdNewsSources.SelectedItem as NewsSource;
            NewsSroucesManager.DeleteNewsSource(selectedNewsSource);
            
            LoadNewsSources();
        }

        private void comboType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem selectedItem = (ComboBoxItem)comboType.SelectedItem;
            //regular expressions are allowed for a crawling start page news source
            if ((NewsSourceType)selectedItem.Tag== NewsSourceType.CrawlingStartPage)
            {
                txtRegularExpression.IsEnabled = true;
            }
            else
            {
                txtRegularExpression.Text = "";
                txtRegularExpression.IsEnabled = false;
            }
        }




        }

       
    }

