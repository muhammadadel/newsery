﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Newsery.Backoffice
{
    /// <summary>
    /// Interaction logic for Newserys.xaml
    /// </summary>
    public partial class NewsControl : UserControl
    {
        public NewsControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (ControlsLoaded)
                return;
            //there is a bug in xaml designer in vs 2012 that occurs when loading the controls in the tab view. This is a work aroud where we ignor loading the controls when in detecting design mode. Otherwise I would have added the controls inside the tabitems
            //in the xaml itself
            var prop = DesignerProperties.IsInDesignModeProperty;
            bool isInDesignMode= (bool)DependencyPropertyDescriptor.FromProperty(prop, typeof(FrameworkElement)).Metadata.DefaultValue;
            if (isInDesignMode)
                return;
            tbItemTags.Content = new TagsControl();
            tbItemNewsPapers.Content = new NewsPapersControl();
            tbItemCountries.Content = new CountriesControl();
            tbItemNewsSources.Content = new NewsSourcesControl();
        }
        private bool ControlsLoaded=false;

       
    }
}
