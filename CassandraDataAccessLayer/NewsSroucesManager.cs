﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;
using CassandraSharp;
using CassandraSharp.CQLPoco;
using CassandraSharp.CQLPropertyBag;
using CassandraSharp.Config;
using System.Web.Script.Serialization;

namespace Newsery.CassandraDataAccessLayer
{
    public class NewsSroucesManager
    {
        #region Countries
        public static List<Country> GetCountries()
        {

            ICluster cluster = CassandraClient.Cluster;
            string setelctCountriesCommand = "Select * from Newsery.Countries ";
            ICqlCommand command = cluster.CreatePocoCommand();
            var countries = command.Execute<Country>(setelctCountriesCommand).AsFuture().Result;

            return countries.ToList();
        }
        public static void AddNewCountry(Country newCountry)
        {
            ICluster cluster = CassandraClient.Cluster;
            var command = cluster.CreatePocoCommand();
            var preparedQuery = command.Prepare("Insert into Newsery.Countries (Name)values (?)");
            preparedQuery.Execute(newCountry).AsFuture().Wait();
        }
        public static void DeleteCountry(Country country)
        {
            ICluster cluster = CassandraClient.Cluster;
            var command = cluster.CreatePocoCommand();
            var preparedQuery = command.Prepare("Delete From Newsery.Countries where Name=?");
            preparedQuery.Execute(country).AsFuture().Wait();
        }
        #endregion

        #region Newpapers

        public static List<Newspaper> GetNewspapers()
        {
            ICluster cluster = CassandraClient.Cluster;
            string setelctNewspapersCommand = "Select * from Newsery.Newspapers ";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            
            IList<PropertyBag> bags = command.Execute(setelctNewspapersCommand).AsFuture().Result;
            List<Newspaper> newspapers = new List<Newspaper>();
            JavaScriptSerializer serializer= new JavaScriptSerializer();
            foreach (PropertyBag bag in bags)
            {
                Newspaper paper = serializer.Deserialize<Newspaper>((string)bag["Data"]);
                newspapers.Add(paper);
            }
            return newspapers.ToList();
        }

        public static List<Newspaper> GetNewsPapersByCountryName(string countryName)
        {
            //create a temp newspaper to hold query parameters
            Newspaper queryNewspaper = new Newspaper();
            queryNewspaper.Country = countryName;

            ICluster cluster = CassandraClient.Cluster;
            string selectNewsPapersCommand = "Select * from Newsery.Newspapers where Country=?";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            PropertyBag parametersBag = new PropertyBag();
            parametersBag["Country"] = countryName;
            var preparedQuery = command.Prepare(selectNewsPapersCommand);
            IList<PropertyBag> resultBags =preparedQuery.Execute(parametersBag).AsFuture().Result;

            List<Newspaper> newspapers = new List<Newspaper>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            foreach (PropertyBag bag in resultBags)
            {
                Newspaper paper = serializer.Deserialize<Newspaper>((string)bag["Data"]);
                newspapers.Add(paper);
            }
            return newspapers.ToList();
        }
        public static void AddNewNewspaper(Newspaper newNewspaper)
        {
            ICluster cluster = CassandraClient.Cluster;
            string selectNewsPapersCommand = "Insert into Newsery.Newspapers (Id,Country,Data) values (?,?,?)";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            var preparedQuery = command.Prepare(selectNewsPapersCommand);
            PropertyBag bag = new PropertyBag();
            bag["Id"] = newNewspaper.Id;
            bag["Country"] = newNewspaper.Country;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            bag["Data"] = serializer.Serialize(newNewspaper);
             preparedQuery.Execute(bag).AsFuture().Wait();
        }

        public static void DeleteNewsPaper(Newspaper selectedNewsPaper)
        {
            ICluster cluster = CassandraClient.Cluster;
            var command = cluster.CreatePocoCommand();
            var preparedQuery = command.Prepare("Delete From Newsery.Newspapers where Id=?");
            preparedQuery.Execute(selectedNewsPaper).AsFuture().Wait();
        }
        #endregion

        #region Tags

        public static void UpdateTag(Tag editedTag)
        {
            ICluster cluster = CassandraClient.Cluster;
            string selectNewsPapersCommand = "Update Newsery.Tags set Data =? where Id=?";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            var preparedQuery = command.Prepare(selectNewsPapersCommand);
            PropertyBag bag = new PropertyBag();
            bag["Id"] = editedTag.Id;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            bag["Data"] = serializer.Serialize(editedTag);
            preparedQuery.Execute(bag).AsFuture().Wait();
        }
        public static List<Tag> GetTags()
        {
            ICluster cluster = CassandraClient.Cluster;
            string setelctTagsCommand = "Select * from Newsery.Tags ";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            IList<PropertyBag> bags = command.Execute(setelctTagsCommand).AsFuture().Result;
            List<Tag> tags = new List<Tag>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            foreach (PropertyBag bag in bags)
            {
                Tag newTag = serializer.Deserialize<Tag>((string)bag["Data"]);
                tags.Add(newTag);
            }
            return tags;
        }
        public static List<Tag> GetTagsByCountryName(string countryName)
        {
            ICluster cluster = CassandraClient.Cluster;
            string selectTagsCommand = "Select * from Newsery.Tags where Country=?";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            PropertyBag bag = new PropertyBag();
            bag["Country"] = countryName;
            var preparedQuery = command.Prepare(selectTagsCommand);
            IList<PropertyBag> bags = preparedQuery.Execute(bag).AsFuture().Result;
            List<Tag> tags = new List<Tag>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            foreach (PropertyBag b in bags)
            {
                Tag newTag = serializer.Deserialize<Tag>((string)b["Data"]);
                tags.Add(newTag);
            }
            return tags;
        }

        public static void AddNewTag(Tag newTag)
        {
            ICluster cluster = CassandraClient.Cluster;
            string selectNewsPapersCommand = "Insert into Newsery.Tags (Id,Country,Data) values (?,?,?)";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            var preparedQuery = command.Prepare(selectNewsPapersCommand);
            PropertyBag bag = new PropertyBag();
            bag["Id"] = newTag.Id;
            bag["Country"] = newTag.Country;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            bag["Data"]= serializer.Serialize(newTag);
            preparedQuery.Execute(bag).AsFuture().Wait();
        }

        public static void DeleteTag(Tag selectedTag)
        {
            ICluster cluster = CassandraClient.Cluster;
            var command = cluster.CreatePocoCommand();
            var preparedQuery = command.Prepare("Delete From Newsery.Tags where Id=?");
            preparedQuery.Execute(selectedTag).AsFuture().Wait();
        }
        #endregion

        #region NewsSources
        public static void AddNewNewsSource(NewsSource newNewsSource)
        {
            //cassandra doesn't allow for null values in columns, so we put an empty GUID
            if (newNewsSource.TagId == null)
                newNewsSource.TagId = new Guid();
            ICluster cluster = CassandraClient.Cluster;
            string selectNewsPapersCommand = "Insert into Newsery.NewsSources (Id,NewspaperId,Country,Data) values (?,?,?,?)";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            PropertyBag parametersBag = new PropertyBag();
            parametersBag["Id"] = newNewsSource.Id;
            parametersBag["NewspaperId"] = newNewsSource.NewsPaperId;
            parametersBag["Country"] = newNewsSource.Country;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            parametersBag["Data"] = serializer.Serialize(newNewsSource);
            var preparedQuery = command.Prepare(selectNewsPapersCommand);
            preparedQuery.Execute(parametersBag).AsFuture().Wait();
        }
        public static List<NewsSource> GetNewsSourcesByNewspaperId(Guid newspaperId)
        {
            ICluster cluster = CassandraClient.Cluster;
            string selectNewsSourcesCommand = "Select * from Newsery.NewsSources where newspaperId=?";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            var preparedQuery = command.Prepare(selectNewsSourcesCommand);
            PropertyBag parametersBag = new PropertyBag();
            parametersBag["newspaperId"] = newspaperId;
            IList<PropertyBag> resultBags = preparedQuery.Execute(parametersBag).AsFuture().Result;
            List<NewsSource> newsSources = new List<NewsSource>();
            JavaScriptSerializer serializer= new JavaScriptSerializer();
            foreach (PropertyBag bag in resultBags)
            {
                NewsSource source = serializer.Deserialize<NewsSource>((string)bag["Data"]);
                newsSources.Add(source);
            }
            return newsSources;
        }

        public static void DeleteNewsSource(NewsSource selectedNewsSource)
        {
            ICluster cluster = CassandraClient.Cluster;
            var command = cluster.CreatePocoCommand();
            var preparedQuery = command.Prepare("Delete From Newsery.NewsSources where Id=?");
            preparedQuery.Execute(selectedNewsSource).AsFuture().Wait();
        }

        /// <summary>
        /// Reture all NewsSources in the database with their tag value and newspaper name properties filled
        /// </summary>
        /// <returns></returns>
        public static List<NewsSource> GetNewsSources()
        {
            ICluster cluster = CassandraClient.Cluster;
            string setelctNewsSourcesCommand = "Select * from Newsery.NewsSources ";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            IList<PropertyBag> resultBags = command.Execute(setelctNewsSourcesCommand).AsFuture().Result;
            List<NewsSource> newsSrouces = new List<NewsSource>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            foreach (PropertyBag bag in resultBags)
            {
                NewsSource source = serializer.Deserialize<NewsSource>((string)bag["Data"]);
                newsSrouces.Add(source);
            }
            return newsSrouces ;
        }
        /// <summary>
        /// Reture all NewsSources in the database with their tag value and newspaper name properties filled
        /// </summary>
        /// <returns></returns>
        public static List<NewsSource> GetNewsSourcesByCountryName(string country)
        {
            ICluster cluster = CassandraClient.Cluster;
            string setelctNewsSourcesCommand = "Select * from Newsery.NewsSources where country=?";
            IPropertyBagCommand command = cluster.CreatePropertyBagCommand();
            var preparedQuery = command.Prepare(setelctNewsSourcesCommand);
            PropertyBag parametersBag = new PropertyBag();
            parametersBag["country"] = country;
            IList<PropertyBag> resultBags = preparedQuery.Execute(parametersBag).AsFuture().Result;
            List<NewsSource> newsSrouces = new List<NewsSource>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            foreach (PropertyBag bag in resultBags)
            {
                NewsSource source = serializer.Deserialize<NewsSource>((string)bag["Data"]);
                newsSrouces.Add(source);
            }
            return newsSrouces;
        }
        #endregion

    }
}
