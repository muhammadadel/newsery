﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;
using System.Text.RegularExpressions;

namespace Newsery.HtmlParser
{
    public class DefaultWebsiteCrawler:IWebsiteCrawler
    {

        public List<Article> Crawl(string startURL, string childLinksRegularExpression)
        {
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            string html = GetHTML(startURL);
            document.LoadHtml(html);
            var linksOnPage = from lnks in document.DocumentNode.Descendants() where lnks.Name == "a" && lnks.Attributes["href"] != null && lnks.InnerText.Trim().Length > 0 select  lnks.Attributes["href"].Value;
                          
            List<Article> articles = new List<Article>();
            foreach (string link in linksOnPage)
            {
                Match match = Regex.Match(link, childLinksRegularExpression);
                if (match.Success)
                {
                    Article newArticle = new Article();
                    newArticle.Url = link;
                    articles.Add(newArticle);
                }
            }
            return articles;
        }
        private string GetHTML(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            return reader.ReadToEnd();
        }
    }
}
