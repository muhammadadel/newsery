﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newsery.DataTypes;

namespace Newsery.Website.Controllers
{
    public class StoryController : Controller
    {
        //
        // GET: /Story/

        public ActionResult Index(string country,string storyId)
        {
            Story requiredStory = NewsManager.GetStoryById(country,storyId);
            if (requiredStory != null)
                return View("Arabic", requiredStory);
            else
                return View("ArabicNotFound");
        }

    }
}
