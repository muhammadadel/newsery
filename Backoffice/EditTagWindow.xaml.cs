﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newsery.DataTypes;
using Newsery.CassandraDataAccessLayer;

namespace Newsery.Backoffice
{
    /// <summary>
    /// Interaction logic for EditTag.xaml
    /// </summary>
    public partial class EditTagWindow : Window
    {
        private Tag _EditedTag;
        public EditTagWindow()
        {
            InitializeComponent();
        }
        public EditTagWindow(Tag editedTag)
        {
            InitializeComponent();
            _EditedTag = editedTag;
            txtOrder.Text = editedTag.Order.ToString();
            txtValue.Text = editedTag.Value;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            _EditedTag.Order = uint.Parse(txtOrder.Text);
            _EditedTag.Value = txtValue.Text;
            NewsSroucesManager.UpdateTag(_EditedTag);
            this.Close();
        }
    }
}
