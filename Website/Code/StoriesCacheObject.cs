﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newsery.DataTypes;

namespace Newsery.Website
{
    public class StoriesCacheObject
    {
        public string Country;
        public string Tag ;
        public DateTime GenerationTime;
        public IEnumerable<Story> Stories;
    }
}