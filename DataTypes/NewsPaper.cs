﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newsery.DataTypes
{
    public class Newspaper
    {
        public Guid Id
        {
            get
            {
                if (_Id==new Guid() )
                {
                    _Id = Guid.NewGuid();
                }
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }
        public string Name { get; set; }
        public string Country { get; set; }
        private Guid _Id;
     }
}
