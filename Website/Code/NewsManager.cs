﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newsery.DataTypes;
using System.Web.SessionState;
using Newsery.TextSearchDAL;
namespace Newsery.Website
{
    public class NewsManager
    {
        private static HttpSessionState Session
        {
            get
            {
                return HttpContext.Current.Session;
            }
        }


        public static IEnumerable<Story> GetFilteredStories(string country)
        {
            IEnumerable<Story> stories = null;
            if (GetStoriesFromCache(country, out stories) == false)
            {
                stories = GenerateFilteredStories(country);
                StoriesCacheObject cacheObject = new StoriesCacheObject();
                cacheObject.Country = country;
                cacheObject.Tag = "";
                cacheObject.GenerationTime = DateTime.Now;
                cacheObject.Stories = stories;
                WeakReference<StoriesCacheObject> reference = new WeakReference<StoriesCacheObject>(cacheObject);
                SetCachedStories(reference);
            }
            return stories;
        }



        private static IEnumerable<Story> GenerateFilteredStories(string country)
        {
            //currently ignore the country, return egypt only
            List<Story> stories = (List<Story>)System.Web.HttpContext.Current.Cache["EgyptStories"];
            if (SessionManager.GetLoggedInUser() != null)
            {
                Newspaper[] excludedNewspapers = SessionManager.GetExcludedNewspapers();
                var excludedNewspapersString = from Newspaper n in excludedNewspapers where n.Country == country select n.Name;
                Tag[] excludedTags = SessionManager.GetExcludedTags();
                var excludedTagsString = from Tag t in excludedTags where t.Country == country select t.Value;
                var rejectedStories = from Story s in stories where s.Tags.Any(val => excludedTagsString.Contains(val)) select s;
                var filteredStoriesByTag = stories.Except(rejectedStories);
                List<Story> requestedStories = new List<Story>();
                foreach (Story s in filteredStoriesByTag)
                {
                    Story clone = s.Copy();
                    clone.Score = 0;
                    clone.Articles = new List<Article>();
                    foreach (Article a in s.Articles)
                    {
                        if (excludedNewspapersString.Contains(a.NewsPaper))
                            continue;
                        clone.Articles.Add(a);
                        clone.Score += a.Score;

                    }
                    if (clone.Articles.Count > 0)
                        requestedStories.Add(clone);
                }
                return requestedStories.OrderByDescending(s => s.GetSlidingExpirationScore());
            }
            else
            {
                return stories;
            }
        }
        private static IEnumerable<Story> GenerateFilteredStories(string country, string tag)
        {
            //currently ignore the country, return egypt only
            List<Story> stories = (List<Story>)System.Web.HttpContext.Current.Cache["EgyptStories"];
            var filteredStoriesByTag = from Story s in stories where s.Tags.Contains(tag) select s;
            if (SessionManager.GetLoggedInUser() != null)
            {
                Newspaper[] excludedNewspapers = SessionManager.GetExcludedNewspapers();
                var excludedNewspapersString = from Newspaper n in excludedNewspapers where n.Country == country select n.Name;

                List<Story> requestedStories = new List<Story>();
                foreach (Story s in filteredStoriesByTag)
                {
                    Story clone = s.Copy();
                    clone.Score = 0;
                    clone.Articles = new List<Article>();
                    foreach (Article a in s.Articles)
                    {
                        if (excludedNewspapersString.Contains(a.NewsPaper))
                            continue;
                        clone.Articles.Add(a);
                        clone.Score += a.Score;
                    }
                    if (clone.Articles.Count > 0)
                        requestedStories.Add(s);
                }
                return requestedStories.OrderByDescending(s => s.GetSlidingExpirationScore());
            }
            else
            {
                return filteredStoriesByTag.OrderByDescending(s => s.GetSlidingExpirationScore());
            }
        }
        private static bool GetStoriesFromCache(string country, out IEnumerable<Story> cachedStoriesArray, string tag = "")
        {
            cachedStoriesArray = null;
            WeakReference<StoriesCacheObject> reference = GetCachedStories();
            if (reference == null)
                return false;
            StoriesCacheObject cachedStories;
            if (!reference.TryGetTarget(out cachedStories))
                return false;
            if (cachedStories.Country != country)
                return false;
            if (cachedStories.Tag != tag)
                return false;
            TimeSpan timePassed = DateTime.Now - cachedStories.GenerationTime;
            if (timePassed.TotalMinutes > 2)
                return false;
            cachedStoriesArray = cachedStories.Stories;
            return true;
        }
        public static IEnumerable<Story> GetFilteredStories(string country, string tag)
        {
            IEnumerable<Story> stories = null;
            if (GetStoriesFromCache(country, out stories, tag) == false)
            {
                stories = GenerateFilteredStories(country, tag);
                StoriesCacheObject cacheObject = new StoriesCacheObject();
                cacheObject.Country = country;
                cacheObject.Tag = tag;
                cacheObject.GenerationTime = DateTime.Now;
                cacheObject.Stories = stories;
                WeakReference<StoriesCacheObject> reference = new WeakReference<StoriesCacheObject>(cacheObject);
                SetCachedStories(reference);
            }
            return stories;
        }
        private static WeakReference<StoriesCacheObject> GetCachedStories()
        {
            return Session["CachedStories"] as WeakReference<StoriesCacheObject>;
        }
        private static void SetCachedStories(WeakReference<StoriesCacheObject> cachedStories)
        {
            Session["CachedStories"] = cachedStories;
        }

        internal static void ClearCache()
        {
            Session["CachedStories"] = null;
        }

        internal static Story GetStoryById(string country, string storyId)
        {
            WeakReference<StoriesCacheObject> reference = GetCachedStories();
            StoriesCacheObject cachedStories;
            if (reference != null && reference.TryGetTarget(out cachedStories) && cachedStories.Country == country)
            {
                var requiredStory = from Story s in cachedStories.Stories where s.Id==storyId select s;
                if (requiredStory.Count() > 0)
                    return requiredStory.First();
            }
            //get the story from elastic search database
            StoriesManager storiesManager = new StoriesManager("egypt");
            Story requiredStroy = storiesManager.GetStoryById(storyId);
            return requiredStroy;

        }
    }
}