﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraSharp;
using CassandraSharp.Config;


namespace Newsery.CassandraDataAccessLayer
{
    public  class CassandraClient
    {
     
        /// <summary>
        /// ICluster is thread safe, one cluster object per real cluster is recommended accross the whole application according to a thread on google groups
        /// </summary>
         public static ICluster Cluster
         {
             get
             {
                 if (_Cluster == null)
                 {
                     
                     XmlConfigurator.Configure();
                     _Cluster = ClusterManager.GetCluster("Newsery");
                 }
                 return _Cluster;
             }
             
         }
         private static ICluster _Cluster;
        
    }
}
