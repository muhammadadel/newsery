﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newsery.DataTypes;

namespace Newsery.HtmlParser
{
    public class NewsSourceParserResolver
    {
        public static INewsSourceParser GetParser(NewsSource source)
        {
            if (source.Type == NewsSourceType.Feed)
                return new SyndicationParser();
            if (source.Url.Contains("http://ara.reuters.com"))
                return new ReutersNewsSouceParser();
            return new CrawlerParser();
        }
    }
}
