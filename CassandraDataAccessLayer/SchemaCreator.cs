﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraSharp;
using CassandraSharp.CQLPoco;
using CassandraSharp.Config;

namespace Newsery.CassandraDataAccessLayer
{
    public class SchemaCreator
    {
        public static void CreateSchema1_0 ()
        {

            ICluster cluster = CassandraClient.Cluster;
            string createKeySpaceCommand = "CREATE KEYSPACE Newsery WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 1}";
            //tables
            string createCountriesCommand = "CREATE TABLE Newsery.Countries (Name text PRIMARY KEY)";
            string createNewspapersCommand = "CREATE TABLE Newsery.Newspapers (Id uuid PRIMARY KEY, Data text, Country text)";
            string createTagsCommand = "CREATE TABLE Newsery.Tags ( Id uuid PRIMARY KEY, Data text, Country text)";
            string createNewsSourcesCommand = "CREATE TABLE Newsery.NewsSources ( Id uuid PRIMARY KEY,NewspaperId uuid, Country text, Data text)";
            string createUsersCommand = "Create Table Newsery.Users (email text Primary KEY, Data text)";
            string createExculedNewspapersCommand = "Create Table Newsery.ExcludedNewspapers (email text Primary Key, Data text)";
            string createSubscribedTagsCommand = "Create Table Newsery.ExcludedTags (email text Primary Key, Data text)";

            //indicies
            string createNewspaperCountryIndex = "CREATE INDEX Newspapers_Country ON Newsery.Newspapers (Country)";
            string createTagsCountryIndex = "CREATE INDEX Tags_Country ON Newsery.Tags (Country)";
            string createNewsSourcesCountryIndex = "CREATE INDEX NewsSources_Country ON Newsery.NewsSources (Country)";
            string createNewsSourcesNewspaperIndex = "CREATE INDEX NewsSources_Newspaper ON Newsery.NewsSources (NewspaperId)";

            //tables
            ICqlCommand command= cluster.CreatePocoCommand();

            command.Execute(createKeySpaceCommand).AsFuture().Wait();
            command.Execute(createCountriesCommand).AsFuture().Wait();
            command.Execute(createNewspapersCommand).AsFuture().Wait();
            command.Execute(createTagsCommand).AsFuture().Wait();
            command.Execute(createNewsSourcesCommand).AsFuture().Wait();
            command.Execute(createUsersCommand).AsFuture().Wait(); 
            command.Execute(createExculedNewspapersCommand).AsFuture().Wait();
            command.Execute(createSubscribedTagsCommand).AsFuture().Wait();
            //indicies
            command.Execute(createNewspaperCountryIndex).AsFuture().Wait();
            command.Execute(createTagsCountryIndex).AsFuture().Wait() ;
            command.Execute(createNewsSourcesCountryIndex).AsFuture().Wait();
            command.Execute(createNewsSourcesNewspaperIndex).AsFuture().Wait();
        }
    }
}
