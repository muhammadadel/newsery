﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Newsery.TextSearchDAL;
namespace Newsery.Backoffice
{
    public class Pool
    {

        private static ConcurrentBag<Indexer> EgyptIndexers = new ConcurrentBag<Indexer>();
        private static ConcurrentBag<Indexer> USIndexers = new ConcurrentBag<Indexer>();
        private static ConcurrentBag<IndexingPipeline> Pipelines = new ConcurrentBag<IndexingPipeline>();

        internal static Indexer GetIndexer(string country)
        {
            if (country == "US")
            {
                Indexer USIndexer;
                if (USIndexers.TryTake(out USIndexer))
                    return USIndexer;
                return new Indexer("us");
            }
            else
            {
                Indexer EgyptIndexer;
                if (EgyptIndexers.TryTake(out EgyptIndexer))
                    return EgyptIndexer;
                return new Indexer("egypt");
            }
        }

        
        internal static void ReturnIndexer(Indexer returnedIndexer)
        {
            if (returnedIndexer.IndexName == "us")
                USIndexers.Add(returnedIndexer);
            else
                EgyptIndexers.Add(returnedIndexer);
        }

        internal static IndexingPipeline GetPipeline()
        {
            IndexingPipeline pipeline;
            if (Pipelines.TryTake(out pipeline))
                return pipeline;
            return new IndexingPipeline();
        }

        internal static void ReturnPipeline(IndexingPipeline pipeline)
        {
            Pipelines.Add(pipeline);
        }
    }
}
