﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Configuration;
using Newsery.DataTypes;

namespace Newsery.TextSearchDAL
{
    /// <summary>
    /// The class responsible for initializing elastic search indicies and mappings
    /// </summary>
    public class Initializer:ElasticSearchClient
    {
        public  void Initialize()
        {
          
            InitializeIndices();
            InitializeMappings();
        }

        private void InitializeMappings()
        {
            //attributes of some fields are not mapped to elastic search unless mapfromattributes is called. It needs to be called at index creation
            IIndicesResponse response = Client.MapFluent<Story>(m => m.MapFromAttributes().SearchAnalyzer("arabic").IndexAnalyzer("arabic").IndexName("egypt").TtlField(t => t.SetDisabled(false).SetDefault("2d")));
            response = Client.MapFluent<Story>(m => m.MapFromAttributes().IndexName("us").TtlField(t => t.SetDisabled(false).SetDefault("2d")));
        }
     
        private  void InitializeIndices()
        {
            IndexSettings settings = new IndexSettings();
            
            settings.NumberOfShards = 1;
            settings.NumberOfReplicas = 1;
            settings["refresh_interval"] = "0.5s";
            
            //IIndicesOperationResponse response = Client.CreateIndex("egypt", settings);
            IIndicesOperationResponse response = Client.CreateIndex("us", settings);
            
          
        }
        
    }
}
